const color = document.querySelectorAll(".btn");

document.addEventListener("keydown", (e) => {
  color.forEach((element) => {
    element.style.backgroundColor = "#000000";
    if (
      element.textContent.toUpperCase() === e.key.toUpperCase() ||
      `key${element.textContent}` === e.code
    ) {
      element.style.backgroundColor = "blue";
    }
  });
});
